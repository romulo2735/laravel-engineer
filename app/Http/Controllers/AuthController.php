<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    /**
     * @param AuthRequest $authRequest
     * @return JsonResponse
     */
    public function login(AuthRequest $authRequest): JsonResponse
    {
        if (!Auth::attempt($authRequest->validated())) {
            return response()->json([
                'status' => 'error',
                'http_code' => Response::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Credentials'
            ], Response::HTTP_UNAUTHORIZED);
        }

        $token = Auth::user()->createToken('personal-access-token')->accessToken;

        return response()->json([
            'token' => $token,
        ]);
    }
}
