<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeStoreRequest;
use App\Jobs\ImportEmployeesJob;
use App\Models\Employee;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class EmployeeController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    /**
     * @param Employee $employee
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    /**
     * @param EmployeeStoreRequest $storeRequest
     * @return void
     */
    public function store(EmployeeStoreRequest $storeRequest)
    {
        $file = $storeRequest->file('file')->getClientOriginalName() . time();
        $storeRequest->file('file')->storeAs('imports/employees/', $file);

        ImportEmployeesJob::dispatch($storeRequest->file, Auth::user());

        response()->json([
            'status' => 'success',
            'http_code' => Response::HTTP_CREATED,
            'message' => 'The file is being processed!'
        ], Response::HTTP_CREATED);
    }

    /**
     * @param Employee $employee
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }
}
