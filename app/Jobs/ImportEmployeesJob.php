<?php

namespace App\Jobs;

use App\Imports\EmployeesImport;
use App\Models\User;
use App\Notifications\EmployeesImportNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Excel as ExcelType;
use Maatwebsite\Excel\Facades\Excel;

class ImportEmployeesJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private string $file;
    private User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $file, User $user)
    {
        $this->file = $file;
        $this->user = $user;
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        Excel::import(new EmployeesImport($this->user), $this->file, null, ExcelType::CSV);

        $this->user->notify(new EmployeesImportNotification($this->user));
    }
}
